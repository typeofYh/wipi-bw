## git

1. 克隆仓库两种协议

- ssh
使用非对称加密实现文件传输
- http
短暂链接的一种协议需要输入用户名和密码 在windows中会保存用户管理凭据中，如果用户名密码修改可以直接修改凭据

```
git clone removeUrl(仓库地址)
```

会在当前目录下生成以仓库名称为名字的文件夹,该文件夹自动跟远程仓库关联，并且受git管理

```
cd 仓库名
```

2. 查看远程仓库地址

```
git remove -v
```

3. 查看文件状态

```
git status
```

远程修改文件

## 冲突
同时修改同一个文件同一行代码时，不确定保留哪次修改就会产生冲突

1. 手动解决冲突从新提交


## 分支
```
git branch //查看本地分支
git branch -a // 查看所有分支
git branch -r // 查看远程分支
git branch <branch name> 新建分支
git checkout <branch name> 切换分支
```


## eslint
作用：javascript代码检测工具 检测js语法比如箭头函数加return 只定义没有修改过值用const定义这种语法规范
1. 新建一个.eslintrc.js文件
2. extends 继承语法包  rules可以自己添加自己的语法配置
3. .eslintignore eslint忽略文件

## bebel
作用：处理js脚本文件。所有js文件要通过babel-loader解析转换成es5语法
1. 判断环境生产环境移除console
2. presets 语法预设
3. plugins babel插件

- 依赖
1. babel-plugin-transform-remove-console  移除console
2. @babel/plugin-proposal-decorators 处理装饰器语法
## prettier 
作用：约定代码风格，比如分号，比如空格，比如缩进
- 依赖
1. preitter
2. eslint-config-prettier 解决eslint和prettier冲突
3. eslint-plugin-prettier eslint编译时结合prettier使用
- 升级eslint@7.28.0

## husky
作用：扩展git hooks 提交之前的动作
新增：.huskyrc
- 依赖
1. husky@4.2.3

## lint-stage
作用：指定文件执行脚本
新增.lintstagedrc
- 依赖
1. lint-staged

## commitlint
检查提交信息格式 git commit -m "信息"
新增commitlint.config.js 配置文件
- 依赖
- @commitlint/cli commitlint指令
- @commitlint/config-conventional  语法配置包

- 提交格式

upd：更新某功能（不是 feat, 不是 fix）

feat：新功能（feature）

fix：修补bug

docs：文档（documentation）

style： 格式（不影响代码运行的变动）

refactor：重构（即不是新增功能，也不是修改bug的代码变动）

test：增加测试

chore：构建过程或辅助工具的变动



### less配置
1. 找到config/webpack.config.js getStyleLoaders方法添加
{
  javascriptEnabled: true,
}
修改
sassRegex =》  lessRegex  1
sassModuleRegex =》 lessModuleRegex 1
sass-loader => less => loader  2

loader配置规则
{
  test://,
  exclude:'',
  use:[{laoder:},{loader:}]
}

less-loader版本问题降到7.x

### 主题颜色配置
antd-theme-generator

1. 新建colorjs文件 通过node执行colorjs文件编译 color.less
2. 在html中引入less,加载less.min.js
3. 在window挂载less对象
4. 切换主题通过window.less.modifyVars方法切换主题

### less sass 全局变量
1. 使用style-resources-loader 
2. 配置
```js
patterns: [
  path.join(__dirname,'../src/style/variables.less')
]
```
3. 在所有less文件中都可以使用变量

### react边界错误监控组件（render）
1. componentDidCache 错误上报
2. static getDerivedStateFromError 返回值必须是一个对象 相当于调用setState

### 发布订阅模式
```
const events = {
  events: {}, // {a:[fn,fn]}
  on(callbackName, callback) {
    if (Array.isArray(this.events[callbackName])) {
      this.events[callbackName].push(callback);
    } else {
      this.events[callbackName] = [callback];
    }
  },
  emit(callbackName, ...arg) {
    if (Array.isArray(this.events[callbackName])) {
      this.events[callbackName].forEach((fn) => {
        fn.apply(events, arg);
      });
    }
  },
  off(callbackName, callback) {
    const index = this.events[callbackName].indexOf(callback);
    this.events[callbackName].splice(index, 1);
  },
};
```

### react跨组件通信
```
const Contxt = React.createContext();
// 根组件
<Contxt.Provider value={}>所有使用状态的子组件</Contxt.Provider>

子组件中通过consumer获取状态
<Contxt.Consumer>
{
  (value) => {
    //value provider提供的value
    return 真正渲染的内容
  }
}
</Contxt.Consumer>
```

### 语言配置

1. 下载react-intl {IntlProvider}
2. 定义语言配置文件
3. 调用IntlProvider 注入 locale 和 messages
4. 子组件使用 FormattedMessage组件 注入 id 
5. 切换语言 调用仓库状态 修改 locale 和 messages 

注意： 仓库根组件要包裹IntlProvider



### 接口统一管理
1. 接口复用
2. 方法复用


### 函数组件和class组件的区别
1. 函数组件没有自己的状态，没有生命周期，直接渲染视图，没有this
2. class 有自己的状态，有生命周期，继承React.component  setState

### useState
让函数组件拥有自己的状态
const [当前状态值, 设置状态的函数] = useState(默认状态/function) function 返回值是默认状态

设置状态的函数传递的参数是当前状态的最新值
