import { Component, Suspense } from "react";
import { Route, Switch, BrowserRouter, HashRouter, Redirect } from "react-router-dom";
import _ from "lodash";
export class Router extends Component {
  renderRouterView = () => {
    const { routerConfig } = this.props;
    const { routes } = routerConfig;
    return <RouterView routes={routes}></RouterView>;
  };
  render() {
    const { mode = "history" } = this.props.routerConfig;
    return mode === "history" ? (
      <BrowserRouter>{this.renderRouterView()}</BrowserRouter>
    ) : (
      <HashRouter>{this.renderRouterView()}</HashRouter>
    );
  }
}
export const beforeEach = ({ meta }) => {
  document.title = meta.title || "八维创作平台";
};
class RouterView extends Component {
  render() {
    const { routes = [] } = this.props;
    const redirectRoutes = routes
      .filter(({ redirect }) => !!redirect)
      .map(({ redirect, path }) => <Redirect key={path} exact from={path} to={redirect} />);
    const comRoutes = routes.filter(({ redirect, path }) => !redirect && path !== "*");
    const commonRoutes = routes.find((item) => item.path === "*");
    return (
      <Suspense fallback={<div className={"loading"}>loading...</div>}>
        <Switch>
          {comRoutes
            .map(({ path, component: Com, routes = [], meta = {}, wrappers: Wrap }) => {
              return (
                <Route
                  path={path}
                  key={path}
                  render={(props) => {
                    beforeEach({ path, meta, routes });
                    if (_.isFunction(Wrap)) {
                      const NewCom = Wrap(Com);
                      return <NewCom {...props} routes={routes} />;
                    }
                    return <Com {...props} routes={routes} />;
                  }}
                />
              );
            })
            .concat(redirectRoutes)}
          {commonRoutes && (
            <Route
              path={commonRoutes.path}
              render={(props) => <commonRoutes.component {...props} />}
            />
          )}
        </Switch>
      </Suspense>
    );
  }
}

export default RouterView;
