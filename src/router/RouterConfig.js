import { lazy } from "react";
import Islogin from "@/components/isLogin";
// import { Intl } from "@/store/modules/pageStore";
export const homeRoutes = [
  {
    path: "/note",
    meta: {
      title: "title.note",
    },
    nav: true,
    wrappers: Islogin,
    component: lazy(() => import(/* webpackChunkName: "note" */ "@/pages/note")),
  },
  {
    path: "/archives",
    meta: {
      title: "title.archives",
    },
    nav: true,
    component: lazy(() => import(/* webpackChunkName: "archives" */ "@/pages/archives")),
  },
  {
    path: "/detail",
    meta: {
      title: "title.archives",
    },
    nav: true,
    component: lazy(() => import(/* webpackChunkName: "archives" */ "@/pages/detail")),
  },
];
export const routes = [
  {
    path: "/",
    component: lazy(() => import(/* webpackChunkName: "layout" */ "@/pages/layout.js")),
    routes: [
      ...homeRoutes,
      {
        path: "*",
        meta: {
          title: "八维创作平台",
        },
        component: lazy(() => import("@/pages/notFond")),
      },
      {
        path: "/",
        redirect: "/note",
      },
    ],
  },
  {
    path: "*",
    meta: {
      title: "八维创作平台",
    },
    component: lazy(() => import("@/pages/notFond")),
  },
];
const oroutes = {
  mode: "history",
  routes,
};

export default oroutes;
