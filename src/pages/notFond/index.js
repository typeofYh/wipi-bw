import { Component } from "react";
import { Result, Button } from "antd";
import style from "./style.module.less";
import classnames from "classnames";

class NotFond extends Component {
  timer = null;
  state = {
    sumTime: 5,
  };
  componentDidMount() {
    this.timer = setInterval(() => {
      let { sumTime } = this.state;
      this.setState(
        {
          sumTime: --sumTime,
        },
        () => {
          if (sumTime <= 0) {
            clearInterval(this.timer);
            this.props.history.replace("/");
          }
        }
      );
    }, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  render() {
    const { sumTime } = this.state;
    return (
      <div className={classnames("container", style.container)}>
        <Result
          status="404"
          title="404"
          subTitle="页面找不到返回首页"
          extra={
            <Button type="primary" onClick={() => this.props.history.replace("/")}>
              点击返回首页，{sumTime}秒自动返回首页
            </Button>
          }
        />
      </div>
    );
  }
}

export default NotFond;
