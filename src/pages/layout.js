import React from "react";
import RouterView from "@/router/RouterViews"; // RouterViews
import { Menu } from "antd";
import { homeRoutes } from "@/router/RouterConfig";
import { NavLink } from "react-router-dom";
import ThemeCom from "@/components/Theme";
import { observer, inject } from "mobx-react";
import { FormattedMessage } from "react-intl";

@inject("pageStore")
@observer
class Layout extends React.Component {
  hanldeChangeLange = () => {
    const curLange = this.props.pageStore.langeData.locale;
    this.props.pageStore.setLange(curLange === "en" ? "zh-CN" : "en");
  };
  render() {
    const {
      routes,
      pageStore: {
        theme: { open },
        langeData,
      },
    } = this.props;
    return (
      <div className="container">
        <header>
          <Menu mode="horizontal" theme={open ? "light" : "dark"}>
            {homeRoutes
              .filter((val) => val.nav)
              .map((item) => {
                return (
                  <Menu.Item key={item.path}>
                    <NavLink to={item.path}>
                      <FormattedMessage id={item.meta.title} />
                    </NavLink>
                  </Menu.Item>
                );
              })}
          </Menu>
          <ThemeCom />
          <button onClick={this.hanldeChangeLange}>{langeData.locale}</button>
        </header>
        <section>
          <RouterView routes={routes} />
        </section>
      </div>
    );
  }
}

export default Layout;
