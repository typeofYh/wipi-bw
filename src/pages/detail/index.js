import "./scroll.css";
import { useEffect, useState, useRef, useCallback } from "react";
// import "./markdown.less";
import detailData from "./data.js";
const tags = ["h1", "h2", "h3", "h4"];
const http = () =>
  new Promise((resove) => {
    setTimeout(() => {
      resove(detailData);
    }, Math.floor(Math.random() * 3) * 1000);
  });
const Scroll = () => {
  const [data, setData] = useState({});
  const [curIndex, setCurIndex] = useState(0);
  const content = useRef(null);
  const container = useRef(null);
  const hAry = useRef([]);
  const events = useRef(null);
  useEffect(() => {
    http().then((res) => {
      setData(res);
    });
  }, []);
  const removeEvent = () => {
    container.current && container.current.removeEventListener("scroll", events.current);
  };
  useEffect(() => {
    events.current = (e) => {
      const newHAry = hAry.current.map((item) => ({
        ...item,
        top: item.el.getBoundingClientRect().top,
      }));
      const curHItemIndex = newHAry.findIndex(
        ({ top }) => top > 0 && top < container.current.offsetHeight / 2
      );
      setCurIndex((curIndex) => (curHItemIndex < 0 ? curIndex : curHItemIndex));
    };
    if (container.current && data.data) {
      removeEvent();
      container.current.addEventListener("scroll", events.current);
      hAry.current = Array.from(content.current.children)
        .filter((val) => {
          return tags.includes(val.tagName.toLowerCase());
        })
        .map((item, index) => ({
          el: item,
          index,
          offsetTop: item.offsetTop,
          top: item.getBoundingClientRect().top,
        }));
    }
    return removeEvent;
  }, [container, data.data]);
  const handleClick = useCallback(
    (index) => {
      setCurIndex(() => index);
      container.current.scrollTop = hAry.current[index].offsetTop;
    },
    [container, hAry]
  );
  return (
    <>
      {data.data ? (
        <div className="container" ref={container}>
          <div
            className="content markdown"
            ref={content}
            dangerouslySetInnerHTML={{ __html: data.data.html }}
          ></div>
          <div className="slidbar">
            {JSON.parse(data.data.toc).map((item, index) => {
              return (
                <div
                  key={item.id}
                  className={index === curIndex ? "active" : ""}
                  onClick={() => handleClick(index)}
                >
                  {item.text}
                </div>
              );
            })}
          </div>
        </div>
      ) : (
        <div>loading...</div>
      )}
    </>
  );
};

export default Scroll;
