import { Component } from "react";
import { inject, observer } from "mobx-react";
import { Intl } from "@/store/modules/pageStore";
import { Card as ParentCard } from "antd";
@inject("pageStore", "noteStore")
@observer
class Note extends Component {
  componentDidMount() {
    const { noteStore } = this.props;
    noteStore.getRecommendList();
    noteStore.getNoteList({
      articleStatus: "publish",
    });
  }
  render() {
    const { noteStore } = this.props;
    return (
      <div>
        <ParentCard
          title={Intl.formatMessage({ id: "block.title.recommend" })}
          loading={!noteStore.recommendList.length}
        >
          {noteStore.recommendList.map((item) => (
            <p key={item.id}>{item.title}</p>
          ))}
        </ParentCard>
        <ParentCard
          title={Intl.formatMessage({ id: "block.title.note" })}
          loading={!noteStore.recommendList.length}
        >
          {noteStore.noteList.map((item) => (
            <p key={item.id}>
              <span>{item.label}</span>
              <b>[{item.articleCount}]</b>
            </p>
          ))}
        </ParentCard>
      </div>
    );
  }
}

export default Note;
