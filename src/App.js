import { Component } from "react";
import routerConfig from "@/router/RouterConfig";
import { Router } from "@/router/RouterViews";
import { observer, inject } from "mobx-react";
import { IntlProvider } from "react-intl";

@inject("pageStore")
@observer
class App extends Component {
  render() {
    return (
      <IntlProvider {...this.props.pageStore.langeData} key={this.props.pageStore.langeData.locale}>
        <Router routerConfig={routerConfig} />
      </IntlProvider>
    );
  }
}

export default App;
