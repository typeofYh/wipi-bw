const note = {
  getRecommend: {
    method: "get",
    url: "/api/article/recommend",
  },
  getArticleList: {
    method: "get",
    url: "/api/article",
  },
  getTagList: {
    method: "get",
    url: "/api/tag",
  },
};

export default note;
