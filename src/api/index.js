import axios from "@/utils/httpTool";
import note from "./modules/note";
import { message } from "antd";
const http = axios({
  timeout: 10,
  commonHeaders: {
    token: window.localStorage.getItem("token"),
  },
  failMessage(msg) {
    message.error(msg);
  },
});

const res = Object.keys(note).reduce((def, key) => {
  def[key] = (data = {}) =>
    http({
      ...note[key], //method:'get', url: , params , data
      [note[key].method === "get" ? "params" : "data"]: data,
    });
  return def;
}, {});

export default res;
