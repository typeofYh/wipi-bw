import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./style/common.less";
import store from "@/store";
import { Provider } from "mobx-react";
import "antd/dist/antd.css";
import ErrorBoundary from "@/components/ErrorBoundary";

if (process.env.NODE_ENV === "development") {
  window._store = store;
}

ReactDOM.render(
  <React.StrictMode>
    <ErrorBoundary>
      {/* <ThemeProvider> */}
      <Provider {...store}>
        <App />
      </Provider>
      {/* </ThemeProvider> */}
    </ErrorBoundary>
  </React.StrictMode>,
  document.getElementById("root")
);

/**
 * 1. 下载react-intl
 * 2. 创建语言配置文件（key要保持一质）
 * 3. 调用 IntlProvider 提供语言配置 入口文件中调用 提供语言默认配置 messages={zh-cn:{},ch-en:{}}
 * 4. 只要使用语言引入FormattedMessage 传id
 */
