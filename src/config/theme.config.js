export const dark = {
  "@bg-color": "#000",
  "@text-color": "#999",
};

export const light = {
  "@bg-color": "#fff",
  "@text-color": "#333",
};
