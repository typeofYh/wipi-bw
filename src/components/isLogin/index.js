import { Component } from "react";
import { Redirect } from "react-router-dom";
const Islogin = (Com) => {
  console.log("Islogin", Com);
  return class IsLoginCom extends Component {
    state = {
      Userflag: true,
    };
    componentDidMount() {
      console.log("islogin");
      this.setState({
        Userflag: window.localStorage.getItem("token"),
      });
    }
    render() {
      return this.state.Userflag ? <Com {...this.props} /> : <Redirect to={"/login"} />;
    }
  };
};

export default Islogin;
