import { Component } from "react";

import { observer, inject } from "mobx-react";
@inject("pageStore")
@observer
class Theme extends Component {
  changeTheme = () => {
    this.props.pageStore.setTheme(!this.props.pageStore.theme.open);
  };
  render() {
    const { open } = this.props.pageStore.theme;
    return <button onClick={this.changeTheme}>{open ? "白" : "黑"}</button>;
  }
}

export default Theme;
