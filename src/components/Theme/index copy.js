import { Component, createContext } from "react";
import { dark, light } from "@/config/theme.config.js";

const events = {
  events: {}, // {a:[fn,fn]}
  on(callbackName, callback) {
    if (Array.isArray(this.events[callbackName])) {
      this.events[callbackName].push(callback);
    } else {
      this.events[callbackName] = [callback];
    }
  },
  emit(callbackName, ...arg) {
    if (Array.isArray(this.events[callbackName])) {
      this.events[callbackName].forEach((fn) => {
        fn.apply(events, arg);
      });
    }
  },
  off(callbackName, callback) {
    const index = this.events[callbackName].indexOf(callback);
    this.events[callbackName].splice(index, 1);
  },
};

const data = {
  open: true,
  changeTheme(val) {
    events.emit("changeTheme", val);
  },
};

export const ThemeContext = createContext(data);

export const ThemeProvider = (props) => {
  return <ThemeContext.Provider value={data}>{props.children}</ThemeContext.Provider>;
};
export const connectTheme = (Com) => {
  return class extends Component {
    state = {
      open: data.open,
    };
    componentDidMount() {
      events.on("changeTheme", (val) => {
        data.open = val;
        this.setState({
          open: val,
        });
      });
    }
    render() {
      return (
        <ThemeContext.Consumer>
          {(value) => {
            return <Com theme={value}></Com>;
          }}
        </ThemeContext.Consumer>
      );
    }
  };
};

@connectTheme
class Theme extends Component {
  changeTheme = () => {
    // this.setState(
    // (val) => ({ open: !val.open }),
    // () => {
    this.props.theme.changeTheme(!this.props.theme.open);
    window.less.modifyVars(this.props.theme.open ? light : dark);
    // }
    // );
  };
  render() {
    // const { open } = this.state;
    return <button onClick={this.changeTheme}>{this.props.theme.open ? "白" : "黑"}</button>;
  }
}

export default Theme;
