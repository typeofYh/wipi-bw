import { Component } from "react";
import { errorHandleUp } from "@/utils/logs";
class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error) {
    // 上报错误信息 往sentry上上报
    errorHandleUp({
      type: "component",
      info: error,
    });
    // logComponentStackToMyService(info.componentStack);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>页面加载出现问题，请刷新重试～</h1>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
