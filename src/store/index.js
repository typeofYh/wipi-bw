import { makeAutoObservable } from "mobx";
const context = require.context("@/store/modules", true, /\.js$/);

const res = context.keys().reduce((def, path) => {
  const key = path.match(/^\.\/(\w+)\.js$/)[1];
  const cont = makeAutoObservable(context(path).default);
  def[key] = cont;
  return def;
}, {});

export default res;
