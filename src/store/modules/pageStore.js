import { dark, light } from "@/config/theme.config.js";
import zhCN from "@/config/langConfig/zh-CN";
import chEN from "@/config/langConfig/ch-EN";
import { createIntl, createIntlCache } from "react-intl";

const langeMessage = {
  en: chEN,
  "zh-CN": zhCN,
};
export const Intl = createIntl(
  {
    locale: navigator.language,
    messages: langeMessage[navigator.language],
  },
  createIntlCache()
);

export const getIntl = (setIntl) => {
  getIntl.setIntl = setIntl;
  setIntl(Intl);
};
class PageStore {
  title = "";
  theme = {
    open: true,
  };
  langeData = Intl;
  setTitle(val = "八维创作平台") {
    this.title = val;
    document.title = this.title;
  }
  setTheme(val) {
    this.theme.open = val;
    window.less.modifyVars(val ? light : dark);
  }
  setLange(locale) {
    Intl.locale = locale;
    Intl.messages = langeMessage[locale];
    this.langeData = {
      locale,
      messages: langeMessage[locale],
    };
  }
}

export default new PageStore();
