import api from "@/api";
class Note {
  recommendList = [];
  noteList = [];
  async getRecommendList() {
    const { data } = await api.getRecommend();
    this.recommendList = data;
  }
  async getNoteList(params) {
    const { data } = await api.getTagList(params);
    this.noteList = data;
  }
}

export default new Note();
