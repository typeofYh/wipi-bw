import axios from "axios";
import httpCode from "./httpCodeMap";
import { errorHandleUp } from "@/utils/logs";

// 1. 设置网络超时
// 2. 区分环境 判断路径，拼接前置路径
// 3. 添加公共参数
// 4. 处理公共错误信息

const createHttp = (option = {}) => {
  const { timeout, commonHeaders, withCredentials, failMessage } = Object.assign(
    {
      timeout: 10,
      commonHeaders: {},
      withCredentials: false, // 跨域是否要携带cookie
      failMessage: (msg) => {
        alert(msg);
      },
    },
    option
  );
  const http = axios.create({
    timeout: timeout * 1000,
    baseURL: process.env.REACT_APP_BASEURL,
    withCredentials,
  });
  // 请求前拦截 在send之前执行 并且第一函数要返回合法的请求字段才能发起请求
  http.interceptors.request.use(
    (config) => {
      config = {
        ...config,
        headers: {
          ...config.headers,
          ...commonHeaders,
        },
      };
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  // 响应之前拦截 请求结果取决函数返回值
  http.interceptors.response.use(
    (response) => {
      // 响应成功执行第一函数
      if (response.data.success) {
        // 成功
        return response.data;
      }
      failMessage(
        response.data.msg || httpCode[response.data.statusCode] || "服务出现未知问题，请刷新重试"
      );
      // 上报错误
      errorHandleUp({
        type: "axios",
        info: {
          url: response.config.url,
          method: response.config.method,
        },
      });
      return Promise.reject(response);
    },
    (error) => {
      if (error.code === "ECONNABORTED") {
        //提示用户网络超时
        failMessage("您当前的网络状态不好，请刷新重试～");
      }
      // 500 404
      // 上报错误
      errorHandleUp({
        type: "axios",
        info: {
          url: error.config.url,
          method: error.config.method,
        },
      });
      return Promise.reject(error);
    } // 响应失败
  );

  return http;
};

export default createHttp;
